##About Real-Estate Project

##About White Board Repo

This is an empty repository, it would not have any source code inside. The purpose of this repo is to share tutorials and reading materials in the Wiki page of this repo.

All the readings and tutorials are in Wiki pages of this repo. There is a button on the left side panel to go there.

**None** of these readings are must-to-read. They are just some related topics.

##Wiki Page & Materials

Click [Here](https://bitbucket.org/realestateua/white-board/wiki/Home) to visit our wiki pages.

##Simple Test Server

http://ec2-54-148-154-88.us-west-2.compute.amazonaws.com:4000/

##Coding Style Guide
Please follow this as much as possible.

[Python](https://google-styleguide.googlecode.com/svn/trunk/pyguide.html)

[Javascript](https://google.github.io/styleguide/javascriptguide.xml)