from django.views import generic
from urls import api, return_json, handle_user_error
from django.core.validators import validate_email

from mysite.app.utils.validators import validate_string_exist
from mysite.users.models import create_user

@api
class User(generic.View):

    url_regex = r'^user/?$'

    @return_json
    def get(self, request):
        return {'data': 'get user'}

    @return_json
    @handle_user_error
    def post(self, request):
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)
        validate_string_exist(email, 'email')
        validate_string_exist(password, 'password')
        validate_email(email)
        create_user(email, email, password)
        return {'data': 'post user'}

    @return_json
    def put(self, request):
        return {'data': 'put user'}

    @return_json
    def patch(self, request):
        return {'data': 'patch user'}

    @return_json
    def delete(self, request):
        return {'data': 'delete user'}

    @return_json
    def trace(self, request):
        return {'data': 'trace user'}

