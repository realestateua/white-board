from django.conf.urls import url
from django.http import HttpResponse
from django.core.exceptions import ValidationError
from django.db import IntegrityError
import json


urlpatterns = []


def api(cls):
    p = url(cls.url_regex, cls.as_view())
    urlpatterns.append(p)
    return cls


def return_json(method):
    def decorator(self, request):
        r = method(self, request)
        return HttpResponse(json.dumps(r), content_type="application/json")

    return decorator


def handle_validation_error(method):
    def decorator(self, request):
        try:
            return method(self, request)
        except ValidationError as ve:
            return {'success': False, 'data': str(ve)}

    return decorator


def handle_database_error(method):
    def decorator(self, request):
        try:
            return method(self, request)
        except IntegrityError as ve:
            return {'success': False, 'data': str(ve)}

    return decorator


def handle_user_error(method):
    def decorator(self, request):
        d1 = handle_validation_error(method)
        d2 = handle_database_error(d1)
        return d2(self, request)
    return decorator
