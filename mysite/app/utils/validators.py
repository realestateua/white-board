from django.core.exceptions import ValidationError


def validate_string_exist(target_str, message):
    if target_str is None:
        raise ValidationError(message + ' not defined')
    if len(target_str) is 0:
        raise ValidationError(message + ' is empty')
