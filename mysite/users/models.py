from django.contrib.auth.models import User


def create_user(name, email, password):
    user = User.objects.create_user(name, email, password)
    user.save()
    return user
